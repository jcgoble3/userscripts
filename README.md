# Userscripts by jcgoble3

A handful of userscripts that I choose to make available to the public.
More may be added in the future.

## Using these scripts

To use these scripts, you will need to install the Tampermonkey browser extension.
Tampermonkey is available for Chrome, Firefox, Edge, and Safari.
(Greasemonkey for Firefox should also work, but has not been tested to confirm.)
The install links below assume that Tampermonkey or a compatible alternative has already been installed.

Unfortunately, there is no good solution for using these scripts on mobile devices;
most existing solutions are all outdated.
The best solution currently is to install Firefox Nightly on Android, which allows all extensions
(unlike the main Firefox mobile browser, which supports only a few hand-selected extensions)
using [these instructions](https://blog.mozilla.org/addons/2020/09/29/expanded-extension-support-in-firefox-for-android-nightly/).
Be aware that Firefox Nightly is the alpha-quality pre-release version of Firefox,
meaning that it is at the bleeding edge of Firefox development.
An alpha release is not just likely to have bugs,
but is virtually guaranteed to have bugs from time to time,
and so it is not generally recommended for daily use.
However, it is nevertheless the best option on mobile.

## Scripts

### mrc-setup.user.js

*Intended for use with the [MyRacingCareer game](https://www.myracingcareer.com/).*

Provides a few quality-of-life improvements to the setup screen.
Specifically, feedback is highlighted with a red background to make it easier to see
(note that this is currently tuned for the blue theme, and may not look as good on the default yellow theme),
and additional buttons are added to each setup element to
increase or decrease the element by 2, 4, 8, or 16 with a single click.
This script doesn't do any work for you,
it just makes it easier and faster for you to do the work yourself.

[Install link](https://gitlab.com/jcgoble3/userscripts/-/raw/main/scripts/mrc-setup.user.js)

### mrc-bitwarden.user.js

*Intended for use with the [MyRacingCareer game](https://www.myracingcareer.com/).*

The MRC login interface annoyingly blanks each field when you click in it.
This interferes with the Bitwarden password manager's autofill feature,
which simulates a click in each field.
This script removes the `onClick` function attached to each field
so that the fields are not blanked when clicked,
allowing Bitwarden (and possibly other password managers) to work correctly.

[Install link](https://gitlab.com/jcgoble3/userscripts/-/raw/main/scripts/mrc-bitwarden.user.js)
