// ==UserScript==
// @name         MRC setup improvements
// @namespace    https://goble.dev/
// @version      1.0.0
// @description  Quality of life improvements to the MRC setup interface, including highlighting of feedback and buttons to change settings by 2, 4, 8, or 16.
// @author       Jonathan Goble (jcgoble3)
// @match        https://www.myracingcareer.com/*/setup/*/
// @icon         https://www.google.com/s2/favicons?domain=myracingcareer.com
// @grant        none
// ==/UserScript==

window.plusValueN = function (obj, n) {
    for (var i = 0; i < n; i++) {
        window.plusValue(obj);
    }
}

window.minusValueN = function (obj, n) {
    for (var i = 0; i < n; i++) {
        window.minusValue(obj);
    }
}

function createButton(type, val, index, word, sign) {
    var div = document.createElement("div");
    div.setAttribute("onclick", word + "ValueN('" + type + "', " + val + ")");
    div.setAttribute("class", word);
    div.style.width = "24px";
    div.style.right = "" + (index * 28 + 4) + "px";
    div.innerHTML = sign + val;
    return div;
}

function createPlusButton(type, val, index) {
    return createButton(type, val, index, "plus", "+");
}

function createMinusButton(type, val, index) {
    return createButton(type, val, index, "minus", "-");
}

document.querySelectorAll(".setup_bar_area").forEach((area) => {
    var box = area.querySelector(".setup_box");
    box.removeAttribute("onclick");

    area.style.marginTop = "18px";
    area.querySelector(".setup_box_cap").style.top = "-18px";

    var type = area.id.substring(5);

    area.insertBefore(createPlusButton(type, 16, 1), box.nextElementSibling);
    area.insertBefore(createPlusButton(type, 8, 2), box.nextElementSibling);
    area.insertBefore(createPlusButton(type, 4, 3), box.nextElementSibling);
    area.insertBefore(createPlusButton(type, 2, 4), box.nextElementSibling);
    area.insertBefore(createMinusButton(type, 2, 5), box.nextElementSibling);
    area.insertBefore(createMinusButton(type, 4, 6), box.nextElementSibling);
    area.insertBefore(createMinusButton(type, 8, 7), box.nextElementSibling);
    area.insertBefore(createMinusButton(type, 16, 8), box.nextElementSibling);
});

document.querySelectorAll(".setup_advice").forEach((adviceNode) => {
    if (!adviceNode.innerText.includes("OK")) {
        adviceNode.style.backgroundColor = "darkred";
        adviceNode.previousElementSibling.style.backgroundColor = "darkred";
        var text = adviceNode.previousElementSibling.querySelector(".setup_current_txt");
        text.style.color = "white";
    }
});
